package net.utils.hessian;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * @author Usenko Ievgen.
 */
@RunWith(MockitoJUnitRunner.class)
public class HessianServerTest {

    HessianServer server;

    @Mock
    HelloService firstServiceDelegate;

    @Mock
    HelloService secondServiceDelegate;

    @Before
    public void up() throws Exception {
        server = HessianServer.create()
                .addService(HelloService.class, firstServiceDelegate, "/first")
                .addService(HelloService.class, secondServiceDelegate, "/second")
                .get();
        server.start();
    }

    @Test
    public void testDeploy() throws Exception {
        when(firstServiceDelegate.hello()).thenReturn("a");
        when(secondServiceDelegate.hello()).thenReturn("b");

        HelloService first = new HessianClient<HelloService>(HelloService.class, "http://127.0.0.1:8080/first").get();
        HelloService second = new HessianClient<HelloService>(HelloService.class, "http://127.0.0.1:8080/second").get();

        assertNotNull("a", first.hello());
        assertNotNull("b", second.hello());
    }

    @After
    public void down() {
        server.stop();
    }
}
