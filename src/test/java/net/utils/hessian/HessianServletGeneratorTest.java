package net.utils.hessian;

import com.caucho.hessian.server.HessianServlet;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

/**
 * @author Usenko Ievgen.
 */
public class HessianServletGeneratorTest {

    @Test
    public void testGenerateClassName() {
        final String expected = "TestInterfaceHessianServlet";
        final String result = new HessianServletGenerator(null).generateClassName(TestInterface.class);
        assertEquals(expected, result);
    }

    @Test
    public void testGenerateClassDeclaration() {
        final String className = "TestInterfaceHessianServlet";
        final String expected =
                "public class TestInterfaceHessianServlet extends " + HessianServlet.class.getName() + " implements " + TestInterface.class.getName();
        final String result = new HessianServletGenerator(null).generateClassDeclaration(className, TestInterface.class);
        assertEquals(expected, result);
    }

    @Test
    public void testGenerateDelegateVariableDeclaration() {
        final String variableName = "delegate";
        final String expected = "final TestInterface delegate ;";
        final String result = new HessianServletGenerator(null).generateDelegateVariableDeclaration(TestInterface.class, variableName);
        assertEquals(expected, result);
    }

    @Test
    public void testGenerateConstructor() {
        final String className = "TestInterfaceHessianServlet";
        final String variableName = "delegate";
        final String expected = "public TestInterfaceHessianServlet(TestInterface delegate){this.delegate=delegate;}";
        final String result = new HessianServletGenerator(null).generateConstructor(TestInterface.class, className, variableName);
        assertEquals(expected, result);
    }

    @Test
    public void testGenerateMethodWithoutParams() {
        final String expected = "public java.lang.Integer methodA(){return delegate.methodA();}";
        final Method method = findMethodByName("methodA", TestInterface.class);
        final String result = new HessianServletGenerator(null).generateMethod(method, "delegate");
        assertEquals(expected, result);
    }

    @Test
    public void testGenerateVoidMethodWithoutParams() {
        final String expected = "public void methodC(){delegate.methodC();}";
        final Method method = findMethodByName("methodC", TestInterface.class);
        final String result = new HessianServletGenerator(null).generateMethod(method, "delegate");
        assertEquals(expected, result);
    }

    @Test
    public void testGenerateMethodWithParams() {
        final String expected = "public java.lang.Integer methodB(java.lang.Boolean p0,java.lang.Integer p1){return delegate.methodB(p0,p1);}";
        final Method method = findMethodByName("methodB", TestInterface.class);
        final String result = new HessianServletGenerator(null).generateMethod(method, "delegate");
        assertEquals(expected, result);
    }

    @Test
    public void testGenerateVoidMethodWithParams() {
        final String expected = "public void methodD(java.lang.Boolean p0,java.lang.Boolean p1){delegate.methodD(p0,p1);}";
        final Method method = findMethodByName("methodD", TestInterface.class);
        final String result = new HessianServletGenerator(null).generateMethod(method, "delegate");
        assertEquals(expected, result);
    }

    @Test
    public void testGenerateJavaFile() {
        final HessianServletGenerator gen = new HessianServletGenerator(TestInterface.class);
        final File file = gen.generateJavaFile("A");
        assertTrue(file.exists());
    }

    @Test
    public void testGenerate() {
        final HessianServletDescriptor descriptor = HessianServletGenerator.generate(TestInterface.class);
        assertNotNull(descriptor);
        assertNotNull(descriptor.servletClass);
        assertNotNull(descriptor.servletFile);
        assertTrue(descriptor.servletFile.exists());
    }

    static Method findMethodByName(final String methodName, final Class classToProcess) {
        for (Method method : classToProcess.getMethods()) {
            if (method.getName().equals(methodName)) return method;
        }
        throw new IllegalArgumentException("Invalid method name [" + methodName + "]");
    }
}
