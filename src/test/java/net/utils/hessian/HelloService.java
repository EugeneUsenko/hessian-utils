package net.utils.hessian;

/**
 * @author Usenko Ievgen.
 */
public interface HelloService {
    String hello();
}
