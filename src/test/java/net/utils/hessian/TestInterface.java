package net.utils.hessian;

/**
 * @author Ievgen Usenko
 */
public interface TestInterface {

    Integer methodA();

    Integer methodB(Boolean bool, Integer a);

    void methodC();

    void methodD(Boolean a, Boolean b);
}
