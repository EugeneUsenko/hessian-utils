package net.utils.hessian;

import com.caucho.hessian.server.HessianServlet;
import com.google.common.base.Joiner;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.logging.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Usenko Ievgen.
 */
final class HessianServletGenerator {
    final static Logger log = Logger.getLogger(HessianServletGenerator.class.getSimpleName());

    final static String KEY_TMP_DIR = "java.io.tmpdir";
    final static String TMP_DIR = System.getProperty(KEY_TMP_DIR);

    final static String KEYWORD_PUBLIC = "public";
    final static String KEYWORD_CLASS = "class";
    final static String KEYWORD_EXTENDS = "extends";
    final static String KEYWORD_IMPLEMENTS = "implements";
    final static String KEYWORD_FINAL = "final";
    final static String KEYWORD_THIS = "this";
    final static String KEYWORD_RETURN = "return";
    final static String KEYWORD_DOT = ".";
    final static String KEYWORD_EQ = "=";
    final static String KEYWORD_SEMICOLON = ";";

    final static JavaCompiler COMPILER = ToolProvider.getSystemJavaCompiler();

    final Class hessianService;

    HessianServletGenerator(Class hessianService) {
        this.hessianService = hessianService;
    }

    static HessianServletDescriptor generate(final Class hessianServiceInterface) {
        checkNotNull(hessianServiceInterface, "Interface must not be null");

        final HessianServletGenerator generator = new HessianServletGenerator(hessianServiceInterface);
        final String className = generator.generateClassName(hessianServiceInterface);
        final File hessianServletJavaFile = generator.generateJavaFile(className);


        log.info(hessianServiceInterface + ": generated servlet " + hessianServletJavaFile.getAbsolutePath());

        try {
            int result = COMPILER.run(null, null, null, hessianServletJavaFile.getPath());
            if (result != 0) {
                log.info(hessianServiceInterface + ":" + hessianServletJavaFile + " compilation error, code=" + result);
                throw new RuntimeException("Compilation error: " + hessianServletJavaFile.getAbsolutePath());
            }
            log.info(hessianServiceInterface + ":" + hessianServletJavaFile + " compiled successfully");

            URLClassLoader classLoader = URLClassLoader.newInstance(new URL[]{hessianServletJavaFile.getParentFile().toURI().toURL()});
            Class<?> hessianServletClass = Class.forName(className, true, classLoader);
            log.info(hessianServiceInterface + ": " + hessianServletClass + " loaded");

            return new HessianServletDescriptor(hessianServletJavaFile, hessianServletClass);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    File generateJavaFile(final String generatedClassName) {
        final String delegateVariableName = "delegate";

        final String classDeclaration = generateClassDeclaration(generatedClassName, hessianService);
        final String variableDeclaration = generateDelegateVariableDeclaration(hessianService, delegateVariableName);
        final String constructor = generateConstructor(hessianService, generatedClassName, delegateVariableName);
        final Collection<String> methods = generateMethods(hessianService, delegateVariableName);

        final File javaFile = new File(TMP_DIR, generatedClassName + ".java");
        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileWriter(javaFile));
            out.append(classDeclaration).append("{").println();
            out.append(variableDeclaration).println();
            out.append(constructor).println();
            for (String method : methods) {
                out.append(method).println();
            }
            out.append("}");
        } catch (IOException e) {
            throw new RuntimeException("Could not write generate java file", e);
        } finally {
            IOUtils.safeClose(out);
        }
        return javaFile;
    }

    String generateClassName(final Class hessianServiceInterface) {
        return hessianServiceInterface.getSimpleName() + "HessianServlet";
    }

    String generateClassDeclaration(final String generatedClassName, final Class hessianServiceInterface) {
        return Joiner.on(" ").join(KEYWORD_PUBLIC, KEYWORD_CLASS, generatedClassName,
                KEYWORD_EXTENDS, HessianServlet.class.getName(),
                KEYWORD_IMPLEMENTS, hessianServiceInterface.getCanonicalName());
    }

    String generateDelegateVariableDeclaration(final Class hessianServiceInterface, final String variableName) {
        return Joiner.on(" ").join(KEYWORD_FINAL, hessianServiceInterface.getName(), variableName, KEYWORD_SEMICOLON);
    }

    String generateConstructor(final Class hessianServiceInterface, final String generatedClassName, final String variableName) {
        StringBuilder sb = new StringBuilder();
        sb.append(KEYWORD_PUBLIC).append(" ").append(generatedClassName);
        sb.append("(").append(hessianServiceInterface.getName()).append(" ").append(variableName).append("){");
        sb.append(KEYWORD_THIS).append(KEYWORD_DOT).append(variableName).append(KEYWORD_EQ).append(variableName).append(KEYWORD_SEMICOLON);
        sb.append("}");

        return sb.toString();
    }

    Collection<String> generateMethods(final Class hessianServiceInterface, final String variableName) {
        final Method[] methods = hessianServiceInterface.getMethods();
        if (methods.length == 0) return Collections.EMPTY_LIST;

        final Collection<String> generated = new ArrayList<>(methods.length);
        for (Method method : methods) {
            generated.add(generateMethod(method, variableName));
        }
        return generated;
    }

    String generateMethod(final Method method, final String delegateName) {
        final String methodName = method.getName();
        final boolean isVoid = method.getReturnType().equals(void.class);
        final Collection<Argument> arguments = getArguments(method);

        StringBuilder methodBuilder = new StringBuilder();
        methodBuilder.append(KEYWORD_PUBLIC).append(" ");
        methodBuilder.append(method.getReturnType().getName()).append(" ").append(methodName);

        methodBuilder.append("(");
        methodBuilder.append(Argument.toDeclarationString(arguments));
        methodBuilder.append(")");

        methodBuilder.append("{");

        if (!isVoid) {
            methodBuilder.append(KEYWORD_RETURN).append(" ");
        }
        methodBuilder.append(delegateName).append(".");
        methodBuilder.append(methodName);
        methodBuilder.append("(");
        methodBuilder.append(Argument.toCallString(arguments));
        methodBuilder.append(");");

        methodBuilder.append("}");

        return methodBuilder.toString();
    }

    Collection<Argument> getArguments(final Method method) {
        final Class[] params = method.getParameterTypes();
        if (params.length == 0) return Collections.EMPTY_LIST;

        final Collection<Argument> arguments = new ArrayList<>(params.length);
        for (int i = 0; i < params.length; i++) {
            arguments.add(new Argument(params[i].getName(), "p" + i));
        }
        return arguments;
    }


    static final class Argument {
        final String type;
        final String name;

        public Argument(String type, String name) {
            this.name = name;
            this.type = type;
        }

        static String toDeclarationString(Collection<Argument> arguments) {
            if (arguments.isEmpty()) return "";

            final StringBuilder sb = new StringBuilder();
            for (Argument argument : arguments) {
                sb.append(",").append(argument.type).append(" ").append(argument.name);
            }
            return sb.toString().substring(1);
        }

        static String toCallString(Collection<Argument> arguments) {
            if (arguments.isEmpty()) return "";

            final StringBuilder sb = new StringBuilder();
            for (Argument argument : arguments) {
                sb.append(",").append(argument.name);
            }
            return sb.toString().substring(1);
        }
    }
}
