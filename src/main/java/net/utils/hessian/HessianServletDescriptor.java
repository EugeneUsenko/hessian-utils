package net.utils.hessian;

import java.io.File;

/**
 * Class represents meta information about hessian servlet
 *
 * @author Usenko Ievgen.
 */
final class HessianServletDescriptor {
    final File servletFile;
    final Class servletClass;

    public HessianServletDescriptor(File servletFile, Class servletClass) {
        this.servletFile = servletFile;
        this.servletClass = servletClass;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("HessianServletDescriptor [");
        sb.append("file=").append(servletFile.getAbsolutePath());
        sb.append(", class=").append(servletClass.getName()).append("]");
        return sb.toString();
    }
}
