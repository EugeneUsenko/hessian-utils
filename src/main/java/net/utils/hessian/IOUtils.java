package net.utils.hessian;

import java.io.IOException;
import java.io.Writer;

/**
 * @author Ievgen Usenko
 */
public final class IOUtils {
    private IOUtils() {
    }

    public static void safeClose(final Writer writer) {
        if (writer == null) return;

        try {
            writer.close();
        } catch (IOException e) {
            /* ignore it */
        }
    }
}
