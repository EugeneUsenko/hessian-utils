package net.utils.hessian;

import com.caucho.hessian.client.HessianProxyFactory;

import java.net.MalformedURLException;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * @author Usenko Ievgen.
 */
public final class HessianClient<T> {
    final Class<T> serviceClass;
    final String url;

    public HessianClient(final Class<T> serviceClass, final String url) {
        checkNotNull(serviceClass, "Service class must not be null");
        checkArgument(!isNullOrEmpty(url), "URL must not be null or empty");
        this.serviceClass = serviceClass;
        this.url = url;
    }

    public T get() {
        try {
            HessianProxyFactory factory = new HessianProxyFactory();
            return (T) factory.create(serviceClass, url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
