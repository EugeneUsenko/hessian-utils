package net.utils.hessian;

/**
 * Class represents meta information about hessian service.
 *
 * @author Usenko Ievgen.
 */
final class HessianServiceDescriptor<T> {
    /**
     * Java interface which represents hessian service.
     */
    final Class<T> serviceInterface;

    /**
     * Delegate for the hessian servlet. All service call will be redirected to this instance.
     */
    final T serviceDelegate;

    /**
     * URL pattern used to map the service.
     */
    final String urlPattern;

    HessianServiceDescriptor(final Class<T> serviceInterface,
                             final T serviceDelegate,
                             final String urlPattern) {
        this.serviceInterface = serviceInterface;
        this.serviceDelegate = serviceDelegate;
        this.urlPattern = urlPattern;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HessianServiceDescriptor that = (HessianServiceDescriptor) o;

        if (serviceDelegate != null ? !serviceDelegate.equals(that.serviceDelegate) : that.serviceDelegate != null)
            return false;
        if (serviceInterface != null ? !serviceInterface.equals(that.serviceInterface) : that.serviceInterface != null)
            return false;
        if (urlPattern != null ? !urlPattern.equals(that.urlPattern) : that.urlPattern != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = serviceInterface != null ? serviceInterface.hashCode() : 0;
        result = 31 * result + (serviceDelegate != null ? serviceDelegate.hashCode() : 0);
        result = 31 * result + (urlPattern != null ? urlPattern.hashCode() : 0);
        return result;
    }
}
