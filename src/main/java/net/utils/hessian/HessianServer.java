package net.utils.hessian;

import com.caucho.hessian.server.HessianServlet;
import com.google.common.base.Joiner;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Logger;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Class represents server which publishes hessian services.
 *
 * @author Usenko Ievgen.
 */
public final class HessianServer {
    final static Logger log = Logger.getLogger(HessianServer.class.getSimpleName());

    /**
     * Default value of server context.
     */
    public static final String DEFAULT_CONTEXT = "/";

    /**
     * Default value of server port.
     */
    public static final int DEFAULT_PORT = 8080;

    final int port;
    final String context;
    final Collection<HessianServiceDescriptor> serviceDescriptors;

    Server server;

    private HessianServer(final int port, final String context, final Collection<HessianServiceDescriptor> serviceDescriptors) {
        this.port = port;
        this.context = context;
        this.serviceDescriptors = serviceDescriptors;
    }

    /**
     * Creates new instance of {@link HessianServer.Builder} class.
     *
     * @param port    port number which will be used by server, the value must be grater then {@code 0}.
     * @param context name of the context, which will be used by server, must not be {@code null} or empty.
     * @return new instance of {@link HessianServer.Builder}.
     * @throws java.lang.IllegalArgumentException if given port number if less or equal {@code 0}, if {@code context}
     *                                            is {@code null} or empty.
     */
    public static Builder create(final int port, final String context) {
        checkArgument(port > 0, "The value of port number must be grater then 0, port=" + port);
        checkArgument(!isNullOrEmpty(context), "Context value must not be null or empty");
        return new Builder(port, context);
    }

    /**
     * Creates new instance of {@link HessianServer.Builder} class with default server context.
     *
     * @param port ort number which will be used by server, the value must be grater then {@code 0}.
     * @return new instance of {@link HessianServer.Builder}.
     * @throws java.lang.IllegalArgumentException if given port number if less or equal {@code 0}.
     * @see #DEFAULT_CONTEXT
     * @see #create(int, String)
     */
    public static Builder create(final int port) {
        return create(port, DEFAULT_CONTEXT);
    }

    /**
     * Creates new instance of {@link HessianServer.Builder} class with default server port value.
     *
     * @param context name of the context, which will be used by server, must not be {@code null} or empty.
     * @return new instance of {@link HessianServer.Builder}.
     * @throws java.lang.IllegalArgumentException if {@code context} is {@code null} or empty.
     * @see #DEFAULT_PORT
     * @see #create(int, String)
     */
    public static Builder create(final String context) {
        return create(DEFAULT_PORT, context);
    }

    /**
     * Creates new instance of {@link HessianServer.Builder} class with default server context and
     * port value.
     *
     * @return new instance of {@link HessianServer.Builder}.
     * @see #DEFAULT_PORT
     * @see #DEFAULT_CONTEXT
     * @see #create(int, String)
     */
    public static Builder create() {
        return create(DEFAULT_PORT, DEFAULT_CONTEXT);
    }

    /**
     * Starts server and publishes all registered services.
     */
    public synchronized void start() throws Exception {
        server = new Server(port);

        ServletContextHandler servletContext = new ServletContextHandler(ServletContextHandler.SESSIONS);
        servletContext.setContextPath(context);
        server.setHandler(servletContext);

        for (HessianServiceDescriptor serviceDescriptor : serviceDescriptors) {
            final HessianServletDescriptor servletDescriptor = HessianServletGenerator.generate(serviceDescriptor.serviceInterface);
            final HessianServlet servletInstance = (HessianServlet) servletDescriptor.servletClass.getDeclaredConstructors()[0].newInstance(serviceDescriptor.serviceDelegate);

            servletContext.addServlet(new ServletHolder(servletInstance), serviceDescriptor.urlPattern);

            final String serviceName = serviceDescriptor.serviceInterface.getName();
            final String serviceDelegate = serviceDescriptor.serviceDelegate.getClass().getName();
            final String url = "http://127.0.0.1:" + port + (DEFAULT_CONTEXT.equals(context)
                    ? serviceDescriptor.urlPattern
                    : context + serviceDescriptor.urlPattern);
            log.info(Joiner.on(" ").join(serviceName, "[", serviceDelegate, "]", "->", url));
        }
        server.start();
    }

    /**
     * Stops server.
     *
     * @throws java.lang.IllegalStateException if server has not been started.
     */
    public synchronized void stop() {
        if (server == null) {
            throw new IllegalStateException("Server has not been started");
        }
        try {
            server.stop();
            server.join();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Builder of the {@link HessianServer}
     */
    public static class Builder {
        final int port;
        final String context;
        final Collection<HessianServiceDescriptor> serviceDescriptors = new HashSet<>();

        Builder(int port, String context) {
            this.port = port;
            this.context = context;
        }

        /**
         * Adds service to server.
         *
         * @param serviceInterface interface of the Hessian Service, must not be {@code null}. Interface of the hessian
         *                         service must not be nested class.
         * @param serviceDelegate  delegate of the Hessian Service, must not be {@code null}. All calls from the
         *                         generated hessian servlet will be delegated to this class.
         * @param urlPattern       url prefix which will be used to map current service, must not be {@code null} or
         *                         empty. The meaning of this parameter the same as {@code url-pattern} in the
         *                         regular {@code web.xml} file.
         * @param <T>              type of the hessian service.
         * @return current instance of builder.
         * @throws java.lang.NullPointerException     if passed {@code serviceInterface}, {@code serviceDelegate} or
         *                                            {@code urlPattern} is {@code null}
         * @throws java.lang.IllegalArgumentException if {@code serviceInterface} is regular class, not an interface.
         * @throws java.lang.IllegalArgumentException if {@code serviceInterface} is nested class.
         * @throws java.lang.IllegalArgumentException if {@code urlPattern} is empty or it's not started from slash
         *                                            symbol.
         */
        public <T> Builder addService(final Class<T> serviceInterface,
                                      final T serviceDelegate,
                                      final String urlPattern) {
            checkNotNull(serviceInterface, "Service interface must not be null");
            checkArgument(serviceInterface.isInterface(), "Interface must be specified " + serviceInterface.getName());
            checkArgument(serviceInterface.getName().contains("."), "Interface must not be nested class");
            checkNotNull(serviceDelegate, "Service delegate must not be null");
            checkArgument(!isNullOrEmpty(urlPattern), "URL pattern must not be null or empty");
            checkArgument(urlPattern.startsWith("/"), "URL pattern must start from '/' symbol");

            serviceDescriptors.add(new HessianServiceDescriptor(serviceInterface, serviceDelegate, urlPattern));
            return this;
        }

        /**
         * Creates new instance of the {@link HessianServer}.
         *
         * @return new instance of the {@link HessianServer}.
         */
        public HessianServer get() {
            return new HessianServer(port, context, serviceDescriptors);
        }
    }
}
