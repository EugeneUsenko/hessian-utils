### Description
hessian-utils is a small library which allows dynamically publish [hessian service][hessian] by interface with a custom delegate. Primary purpose 
of the library - quickly setup testing environment which uses a lot of service. There two main classes:

  - HessianServer - allows to publish hessian services
  - HessianClient - allows get hessian service instance(proxy) on the client side
  
### Example

Short example demostrate how to publish service(with the same interface) by two different URLs:

```java
public interface HelloService{
	String hello();
}
```
Server code:
```java 
HessianServer server = HessianServer.create()
    .addService(HelloService.class, firstServiceDelegate, "/first"
    .addService(HelloService.class, secondServiceDelegate, "/second")
    .get();
server.start();  
```
Client code:

```java 
HelloService first = new HessianClient<HelloService>(HelloService.class, "http://127.0.0.1:8080/first").get();
String firstString = first.hello();

HelloService second = new HessianClient<HelloService>(HelloService.class, "http://127.0.0.1:8080/second").get();
String secondString = second.hello();
```
  
[hessian]: http://hessian.caucho.com/index.xtp
[md-editor]: https://stackedit.io/editor







